from django.shortcuts import render, get_object_or_404

from django.template.defaultfilters import slugify
from .models import Post
from taggit.models import Tag


def home_view(request):
	posts= Post.objects.order_by("-id")[:3] # Zobrazení posledních tří příspěvků na hlavní straně
	context = {
		"posts":posts,
	}

	return render(request,"homepage.html",context)

def detail_view(request,slug):
	post = get_object_or_404(Post,slug=slug)
	context = {
		"post":post,
	}
	return render(request,"detail.html",context)

def tag_view(request,slug):
	tag = get_object_or_404(Tag,slug=slug)
	posts = Post.objects.filter(tags=tag)
	context = {
		"tag":tag,
		"posts":posts,
	}

	return render(request,"tagsearch.html",context)